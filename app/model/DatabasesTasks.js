Ext.define('PixiuReceiveagentUi.model.DatabasesTasks', {
  extend: 'Ext.data.Model',

  fields: [
    { name: 'given_id'},
    { name: 'data_source_name', mapping: function(data){
      return data.data_source.name;
    }},
    { name: 'source_tables', mapping: function(data){
       return data.source_tables.join();
    }},
    { name: 'security_policy_name', mapping: function(data){
      if(data.security_policy != null){
        return data.security_policy.name;
      } else {
         return '无限制';
      }
    }},
    { name: 'effectiveTime', mapping: function(data){
      if(data.time_policy != null){
        return data.time_policy.name;
      } else {
         return '无限制';
      }
    }},
    { name: 'blocked'},
    { name: 'transfered'},
    { name: 'failed'},
    { name: 'threshold'},
    { name: 'mome'},

    { name: 'sync_mode', mapping: function(data){
       if(data.sync_mode == "FULL"){
          return '全表同步';
       } else {
          return '增量同步';
       }
    }},

    { name: 'virus_scan', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.virus_scan){
            return '是';
         } else {
            return '否';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'keyword', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.whitelist != null){
           var keywords = data.security_policy.whitelist.keywords.join();
            return '关键字白名单名称：' + data.security_policy.whitlist.name + '，关键字名单：' + keywords;
         } else if(data.security_policy.blacklist != null) {
           var keywords = data.security_policy.blacklist.keywords.join();
            return '关键字黑名单名称：' + data.security_policy.blacklist.name + '，关键字名单：' + keywords;
         } else {
            return '无限制';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'type', mapping: function(data){
      if(data.time_policy != null){
         if(data.time_policy.run_mode == "ONCE"){
            return '一次';
         } else {
            return '周期';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'time', mapping: function(data){
      if(data.time_policy != null){
         if(data.time_policy.run_mode == "ONCE"){
            return time.starting_at + " - " + time.ending_at;
         } else {
           var timeArray = [];
           var character = [ {"Mon": "星期一" }, {"Tue": "星期二" }, { "Wed": "星期三" }, {"Thu": "星期四" }, {"Fri": "星期五" }, {"Sat": "星期六" }, {"Sun": "星期日" },]
           var week = data.time_policy.time_spans;
           for(i = 0; i < week.length ; i ++ ){
            timeArray.push(character[week[i].wday] + ', 时间 ' + week[i].started_at + ' - ' + week.stopped_at);
           }

           var joinArray = timeArray.join();
           return joinArray;
         }
      } else {
         return '时间无限制';
      }
    }},
  ]
});
