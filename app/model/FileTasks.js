Ext.define('PixiuReceiveagentUi.model.FileTasks', {
  extend: 'Ext.data.Model',

  fields: [
    { name: 'given_id'},
    { name: 'directory'},
    { name: 'effectiveTime', mapping: function(data){
      if(data.time_policy != null){
        return data.time_policy.name;
      } else {
         return '无限制';
      }
    }},
    { name: 'security_policy_name', mapping: function(data){
      if(data.security_policy != null){
        return data.security_policy.name;
      } else {
         return '无限制';
      }
    }},
    { name: 'blocked'},
    { name: 'transfered'},
    { name: 'failed'},
    { name: 'threshold'},
    { name: 'mome'},

    { name: 'label_check', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.label_check){
            return '开启';
         } else {
            return '停用';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'virus_scan', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.virus_scan){
            return '是';
         } else {
            return '否';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'file_size', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.file_size != null ){
           if(data.security_policy.file_size.length != 0 ){
              var file = data.security_policy.file_size;
              return file[0] + 'kb - ' + file[1] + 'kb';
           } else {
              return '无限制';
           }
        } else {
           return '无限制';
        }
      } else {
         return '无限制';
      }
    }},
    { name: 'transfer_time', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.modified_at != null ){
           if(data.security_policy.modified_at.length != 0 ){
              var time = data.security_policy.modified_at;
              return time[0] + ' - ' + time[1];
           } else {
              return '无限制';
           }
        } else {
           return '无限制';
        }
      } else {
         return '无限制';
      }
    }},
    { name: 'code', mapping: function(data){
      if(data.security_policy != null){
        var code = data.security_policy.encodings.join();
        return code;
      } else {
         return '无限制';
      }
    }},
    { name: 'is_allowed', mapping: function(data){
      if(data.security_policy != null){
        if(data.security_policy.is_allowed){
            if(data.security_policy.media_type.length != 0 ){
              var type = data.security_policy.media_type.join();
              return '允许通过' + type + '类型文件';
            } else {
              return '文件允许通过类型无限制';
            }
        } else {
          if(data.security_policy.media_types.length != 0 ){
            var type = data.security_policy.media_types.join();
            return '不允许通过' + type + '类型文件';
          } else {
            return '文件不允许通过类型无限制';
          }
        }
      } else {
         return '无限制';
      }
    }},
    { name: 'file_name_keyword', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.file_name != null){
           var keywords = data.security_policy.file_name.keywords.join();
            return '名称:' + data.security_policy.whitelist.name + '，关键字：' + keywords;
         } else {
            return '无限制';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'keywords', mapping: function(data){
      if(data.security_policy != null){
         if(data.security_policy.whitlist != null){
           var keywords = data.security_policy.whitelist.keywords.join();
            return '关键字白名单名称：' + data.security_policy.whitelist.name + '，关键字名单：' + keywords;
         } else if(data.security_policy.blacklist != null) {
           var keywords = data.security_policy.blacklist.keywords.join();
            return '关键字黑名单名称：' + data.security_policy.blacklist.name + '，关键字名单：' + keywords;
         } else {
            return '无限制';
         }
      } else {
         return '无限制';
      }
    }},

    { name: 'security_level', mapping: function(data){
      if(data.security_policy != null){
         var levels = { "NONE": "无" , "NONSECRET": "非密", "Internal": "内部", "Secret": "秘密", "Confidential": "机密", "TopSecret": "绝密" };
         var grade = data.security_policy.security_level;
         return levels[grade];
      } else {
         return '无限制';
      }
    }},

    { name: 'type', mapping: function(data){
      if(data.time_policy != null){
         if(data.time_policy.run_mode == "ONCE"){
            return '一次';
         } else {
            return '周期';
         }
      } else {
         return '无限制';
      }
    }},
    { name: 'time', mapping: function(data){
      if(data.time_policy != null){
         if(data.time_policy.run_mode == "ONCE"){
            return data.time_policy.starting_at + " - " + data.time_policy.ending_at;
         } else {
           var timeArray = [];
           var character = [ {"MON": "星期一" }, {"TUE": "星期二" }, { "WED": "星期三" }, {"THU": "星期四" }, {"FRI": "星期五" }, {"SAT": "星期六" }, {"SUN": "星期日" },]
           var week = data.time_policy.time_spans;
           for(i = 0; i < week.length ; i ++ ){
            timeArray.push(character[week[i].wday] + ', 时间 ' + week[i].started_at + ' - ' + week.stopped_at);
           }

           var joinArray = timeArray.join();
           return joinArray;
         }
      } else {
         return '时间无限制';
      }
    }},
  ]
});
