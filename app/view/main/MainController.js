/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('PixiuReceiveagentUi.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox',
    ],

    uses: [
        'PixiuReceiveagentUi.view.main.DatabasesTasks',
        'PixiuReceiveagentUi.view.main.FileTasks',
    ],

    alias: 'controller.main',
    
    dbRefresh: function() {
      Ext.getCmp('databases-tasks').getStore().load()
    },

    fileRefresh: function() {
      Ext.getCmp('file-tasks').getStore().load()
    },

    logout: function(){
      Ext.Ajax.request({
        url: 'http://localhost:30600/v1/sign-out',
        //type: 'GET',
        method: 'DELETE',
        Accept: 'application/json',
        ContentType: 'application/json',
        success: function(){
          sessionStorage.clear();
          location.replace(location);
        },
        failure: function(){
          sessionStorage.clear();
          location.replace(location);
        }
      });
    }
});
