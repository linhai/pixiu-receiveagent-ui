Ext.define('PixiuReceiveagentUi.view.main.FileTasksModel', {
  extend: 'Ext.app.ViewModel',

  requires: [
    'PixiuReceiveagentUi.model.FileTasks',
  ],

  alias: 'viewmodel.file_task',

  stores: {
    file_tasks: {
      model: 'PixiuReceiveagentUi.model.FileTasks',
      pageSize: 20,
      autoLoad: true,

      proxy: {
        type: 'ajax',
        url: 'http://localhost:30600/v1/file-transfer-tasks',

        reader: {
          type: 'json',
          rootProperty: 'file_transfer_tasks',
          totalProperty: 'meta.total'
        }
      }
    }
  }
});
