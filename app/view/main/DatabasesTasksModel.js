Ext.define('PixiuReceiveagentUi.view.main.DatabasesTasksModel', {
  extend: 'Ext.app.ViewModel',

  requires: [
    'PixiuReceiveagentUi.model.DatabasesTasks'
  ],

  alias: 'viewmodel.databases_tasks',

  stores: {
    db_tasks: {
      model: 'PixiuReceiveagentUi.model.DatabasesTasks',
      pageSize: 20,
      autoLoad: true,

      proxy: {
        type: 'ajax',
        url: 'http://localhost:30600/v1/db-transfer-tasks',

        reader: {
          type: 'json',
          rootProperty: 'db_transfer_tasks',
          totalProperty: 'meta.total'
        }
      }
    }
  }
});
