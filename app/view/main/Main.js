/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('PixiuReceiveagentUi.view.main.Main', {
    extend: 'Ext.tab.Panel',
    requires: [
      'PixiuReceiveagentUi.view.main.MainController'
    ],
    
    id: 'main',
    items: [{
      title: '文件任务',
      xtype: 'file-tasks'
    }, {
      title: '数据库任务',
      xtype: 'databases-tasks'
    }]
});
