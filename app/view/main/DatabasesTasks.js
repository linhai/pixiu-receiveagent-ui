Ext.define('PixiuReceiveagentUi.view.main.DatabasesTasks', {
  extend: 'Ext.panel.Panel',
  xtype: 'databases-tasks',

  uses: [
    'PixiuReceiveagentUi.view.main.DatabasesTasksModel',
  ],

  viewModel: {
    type: 'databases_tasks'
  },

  overflowY: 'auto',

  controller: 'main',
  padding: '10 0 10 0',
  items: [{
    cls: 'logout',
    xtype: 'button',
    handler: 'logout',
    text: '注销'
  }, {
    cls: 'refresh-button',
    xtype: 'button',
    handler: 'dbRefresh',
    text: '刷新'
  }, {
    defaults: {
      xtype: 'grid',
    },
    items: [{
      id: 'databases-tasks',
      bind: '{db_tasks}',
      columns: [
        { text: '序号', width: 61, xtype: 'rownumberer'},
        { text: '任务号', dataIndex: 'given_id', flex: 1 },
        { text: '数据源', dataIndex: 'data_source_name', flex: 1 },
        { text: '同步模式', dataIndex: 'sync_mode', flex: 1 },
        //{ text: '安全策略', dataIndex: 'security_policy_name', flex: 1 },
        //{ text: '生效时段', dataIndex: 'effectiveTime', flex: 1 },
        { text: '阻断次数', dataIndex: 'blocked', flex: 1 },
        { text: '发送成功数', dataIndex: 'transfered', flex: 1 },
        { text: '发送失败数', dataIndex: 'failed', flex: 1 },
      ],
      plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl: [
          '<table>',
            '<tr>',
              //'<td class="title">数据源</td><td> &nbsp&nbsp数据源名称：{data_source_name}, &nbsp&nbsp代理地址：{data_source.host}, &nbsp&nbsp代理端口：{data_source.port}, &nbsp&nbsp数据库类型：{data_source.db_type}, &nbsp&nbsp数据库名：{data_source.db_name}, &nbsp&nbsp库模式：{data_source.schema}, &nbsp&nbsp用户名：{data_source.user}</td>',
              '<td class="title">数据源</td><td> &nbsp&nbsp数据源名称：{data_source_name},  &nbsp&nbsp数据库类型：{data_source.db_type}, &nbsp&nbsp数据库名：{data_source.db_name}, &nbsp&nbsp库模式：{data_source.schema}</td>',
            '</tr><tr>',
              '<td class="title">同步表</td><td>{source_tables}</td>',
            '</tr><tr>',
              '<td class="title">同步模式</td><td>{sync_mode}</td>',
            //'</tr><tr>',
              //'<td class="title">安全策略</td><td>策略名：{security_policy_name} , &nbsp&nbsp是否开启病毒扫描： {virus_scan}, &nbsp&nbsp过滤关键字：{keywords} </td>',
            //'</tr><tr>',
              //'<td class="title">生效时间</td><td>名称：{effectiveTime}, &nbsp&nbsp周期类型： {type}, {time}</td>',
            '</tr><tr>',
              '<td class="title">时间间隔</td><td>{threshold}秒</td>',
            '</tr><tr>',
              '<td class="title">备注</td><td>{memo}</td>',
            '</tr>',
          '</table>',
        ]
      }]
    }],
  }]
});
