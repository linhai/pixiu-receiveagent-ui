Ext.define('PixiuReceiveagentUi.view.main.FileTasks', {
  extend: 'Ext.panel.Panel',
  xtype: 'file-tasks',

  requires: [
    'PixiuReceiveagentUi.view.main.FileTasksModel',
  ],

  viewModel: {
    type: 'file_task'
  },

  overflowY: 'auto',

  controller: 'main',
  padding: '10 0 10 0',
  items: [{
    cls: 'logout',
    xtype: 'button',
    handler: 'logout',
    text: '注销'
  }, {
    cls: 'refresh-button',
    xtype: 'button',
    handler: 'fileRefresh',
    text: '刷新'
  }, {
    defaults: {
      xtype: 'grid',
    },
    //height: "100%",
    //autoScroll: true,
    items: [{
      id: 'file-tasks',
      bind: '{file_tasks}',
      columns: [
        { text: '序号', width: 61, xtype: 'rownumberer'},
        { text: '任务号', dataIndex: 'given_id', flex: 1 },
        { text: '目录', dataIndex: 'directory', flex: 1},
        //{ text: '安全策略', dataIndex: 'security_policy_name', flex: 1 },
        //{ text: '生效时段', dataIndex: 'effectiveTime', flex: 1 },
        { text: '阻断次数', dataIndex: 'blocked', flex: 1 },
        { text: '发送成功数', dataIndex: 'transfered', flex: 1 },
        { text: '发送失败数', dataIndex: 'failed', flex: 1 },
      ],
      plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl: [
          '<table>',
            '<tr>',
              '<td class="title">目录</td><td>{directory}</td>',
            //'</tr><tr>',
              //'<td class="title">安全策略</td><td>名称：{security_policy_name}, &nbsp&nbsp文件标签： {label_check}, &nbsp&nbsp是否开启病： {virus_scan}, &nbsp&nbsp传送文件大小限制：{file_size}, &nbsp&nbsp传送时间限制：{transfer_time},&nbsp&nbsp编码方式：{code}, &nbsp&nbsp密级：{security_level} </td>',
            //'</tr><tr>',
              //'<td class="title"></td><td>文件类型：{is_allowed}, &nbsp&nbsp文件名过滤： {file_name_keyword}, &nbsp&nbsp过滤关键字：{keywords}</td>',
            //'</tr><tr>',
              //'<td class="title">生效时间</td><td>名称：{effectiveTime.name}, &nbsp&nbsp周期类型： {type}, {time}</td>',
            '</tr><tr>',
              '<td class="title">时间间隔</td><td>{threshold}秒</td>',
            '</tr><tr>',
              '<td class="title">备注</td><td>{memo}</td>',
            '</tr>',
          '</table>',
        ]
      }]
    }],
  }]
});
