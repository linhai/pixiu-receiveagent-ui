/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('PixiuReceiveagentUi.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'PixiuReceiveagentUi'
    }

    //TODO - add data, formulas and/or methods to support your view
});
