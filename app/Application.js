/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('PixiuReceiveagentUi.Application', {
    extend: 'Ext.app.Application',

    name: 'PixiuSendagentUi',
    uses: [
      'Ext.grid.Panel'
    ],

    stores: [
    ],

    launch: function () {
        // TODO - Launch the application
      var token = sessionStorage.getItem('token');
      Ext.getCmp('main').hide();
      if(token == "" || token == undefined || token == null){
        location.replace('login.html');
      } else {
        Ext.getCmp('main').show();
        this.getStatus();
      }

      //if(token) {
        //Ext.Ajax.setDefaultHeaders({
          //'x-user-Token': token
        //});
      //}

    },

    getStatus: function() {
      var _this = this;
      setInterval(function() {
        _this.loginState();
      }, 5000)
    },

    loginState: function() {
      Ext.Ajax.request({
        url: 'http://localhost:30600/v1/status',
        method: "GET",
        Accept: 'application/json',
        ContentType: 'application/json',
        failure: function(reponse) {
          var state = reponse.status;
          if(state != 204){
            Ext.Ajax.request({
              url: 'http://localhost:30600/v1/sign-out',
              method: "DELETE",
              Accept: 'application/json',
              ContentType: 'application/json'
            });
            Ext.Msg.alert("提示", "连接已断开，请重新登录！", function() {
              sessionStorage.clear();
              location.replace(location);
            });
          }
        }
      })
    }
});
